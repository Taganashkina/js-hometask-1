/**
 * 1. Напиши функцию convertCelsiusToFahrenheit,
 * которая принимает на вход число — количество градусов в шкале Цельсия и возвращает число — количество
 * градусов в шкале Фаренгейта. Для перевода градусов Цельсия в градусы Фаренгейта, воспользуйся формулой
 * f = (c × 9/5) + 32, где f — градусы Фаренгейта, c — градусы Цельсия.
 */
function convertCelsiusToFahrenheit (degrees) {
  return (((degrees * 9)/5) + 32);
}

/**
 * 2. Напиши функцию convertStringToNumber, которая принимает на вход строку, и, если строка приводится к числу,
 * то возвращает это число, иначе возвращает false.
 */
function convertStringToNumber (str) {
  if (Number(str)) {
    return Number(str);
  } else {
    return false;
  }
}

/**
 * 3. Напиши функцию getNaN, возвращающую NaN, который должен получаться из строки abc
 * с помощью бинарного или унарного оператора.
 */
function getNaN () {
  return +'abc';
}

/**
 * 4. Напиши функцию createGratitude, которая принимает имя пользователя и оценку — число от 1 до 5,
 * и возвращает строку: {Имя пользователя} оценил вас на {оценка} из 5. Спасибо, {Имя пользователя}!.
 * Если имя не задано, то писать Аноним. Если не задана оценка, то писать 0.
 */
function createGratitude (name, rating) {
  if (name == undefined) {
    name = 'Аноним';
  }
  if (rating == undefined) {
    rating = 0;
  }
  return name + " оценил вас на " + rating + " из 5. Спасибо, " + name + "!";
}

/**
 * 5. Напиши функции checkA1, checkA2, checkA3, которые возвращают значение `a`,
 * если `a` не равен нулю и строку "Все плохо", если `a` равен 0.
 * Сделай это при помощи:
 *   Конструкции if-else.
 *   Тернарного оператора.
 *   Логического или (||).
 */
function checkA1 (a) {
  // 1. if-else
  if(a === 0) {
    return "Всё плохо";
  } else {
    return a;
  }
}

function checkA2 (a) {
  // 2. тернарный оператор
  return a != 0 ? a : "Всё плохо";
}

function checkA3 (a) {
  // 3. логическое или
  return a || "Всё плохо";
}

/**
 * 6. Напиши функцию squaresSum, которая принимает на вход границы диапазона чисел (нижнюю - min, и верхнюю - max)
 * возвращает сумму квадратов всех чисел, входящих в диапазон. Например:
 *  (5,6) → (25 + 36) → 61
 *  (1,4) → (1 + 4 + 9 + 16) → 30
 */
function squaresSum (min, max) {
  var summ = 0;
  for (var i = min; i <= max; i++) {
    summ = summ + i * i;
  }
  return summ;
}

module.exports = {
  convertCelsiusToFahrenheit,
  convertStringToNumber,
  getNaN,
  createGratitude,
  checkA1,
  checkA2,
  checkA3,
  squaresSum
};
